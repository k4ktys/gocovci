#!/bin/bash

go build -o ~/go/bin/gocovci ./cmd/gocovci

# Проверяем, передан ли аргумент (patch версия)
if [ $# -eq 0 ]; then
    echo "Необходимо указать patch версию как аргумент."
    exit 1
fi
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build

# Читаем переданную patch версию
PATCH_VERSION=$1

# Имя образа
IMAGE_NAME="eazzygroup/gocovci"

# Полное имя образа с тегом
FULL_IMAGE_NAME="${IMAGE_NAME}:1.0.${PATCH_VERSION}"

# Сборка Docker образа
echo "Сборка образа ${FULL_IMAGE_NAME}..."
docker build . -t "${FULL_IMAGE_NAME}"

# Публикация образа
echo "Публикация образа ${FULL_IMAGE_NAME}..."
docker push "${FULL_IMAGE_NAME}"