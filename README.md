# gocovci 
Library to check test coverage.

## Instruction for developers
Used in ci/cd pipeline using docker image. 

## Options
```sh
OPTIONS:
    -excluded_pkgs value                    Exclude path from running tests, comma separated
    -excluded_pkgs pkg1,pkg2,dir1/pkg3      Example
```

Example configuration for gitlab:
```yml
stages:
  - test

go_coverage:
  image: eazzygroup/gocovci:latest
  before_script: []
  variables:
    GO_COVERAGE: "20" // expected coverage
  stage: test
  script:
    - cd ./path // folder for which you want to check coverage
    - gocovci 
```
You can view the docker image versions here - https://hub.docker.com/r/eazzygroup/gocovci/tags

