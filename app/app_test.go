package app_test

import (
	"log"
	"os"
	"testing"

	"gitlab.com/valentinsubotka/gocovci/app"
)

func TestAppWithoutEnvVar(t *testing.T) {
	if os.Getenv("TEST_APP_1") == "" {
		os.Setenv("TEST_APP_1", "true")
	} else {
		return
	}

	app := app.NewCoverageApp()
	err := app.Run()

	log.Println("test 1")

	if err == nil {
		t.Errorf("App setup without GO_COVERAGE env var should return error")
	}

	if err != nil {
		if err.Error() != "GO_COVERAGE env var is not set" {
			t.Errorf("Invalid error message: %v", err)
		}
	}

	_ = os.Remove("coverage.out")
}

func TestAppWithEnvVar(t *testing.T) {
	if os.Getenv("TEST_APP_2") == "" {
		os.Setenv("TEST_APP_2", "true")
		os.Setenv("GO_COVERAGE", "70")
	} else {
		return
	}

	app := app.NewCoverageApp()
	err := app.Run()

	log.Println("test 1")

	if err != nil {
		t.Errorf("Error during app.Run(): %v", err)
	}

	if _, err := os.Stat("coverage.out"); err != nil {
		t.Errorf("Missing coverage.out: %v", err)
	}

	_ = os.Remove("coverage.out")
}
