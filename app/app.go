package app

import (
	"flag"
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/joho/godotenv"
	"gitlab.com/valentinsubotka/gocovci"
)

type CoverageApp struct {
	Reader gocovci.CoverageReader
	Writer gocovci.CoverageWriter
}

func NewCoverageApp() *CoverageApp {
	writer := gocovci.NewFileCoverageWriter()
	reader := gocovci.NewFileCoverageReader()

	return &CoverageApp{Reader: reader, Writer: writer}
}

func (c *CoverageApp) Run() error {
	var excludedPkgs string

	flag.StringVar(&excludedPkgs, "excluded_pkgs", "", "excluded packages")
	flag.Parse()

	if _, err := os.Stat("go.mod"); err == nil {
		return c.runDefault()
	}

	log.Println("run recursive")

	if excludedPkgs != "" {
		paths := strings.Split(excludedPkgs, ",")

		excludedPaths := make(map[string]bool)

		for _, path := range paths {
			excludedPaths[path] = true
		}

		return c.runRecursive(excludedPaths)
	} else {
		return c.runRecursive(nil)
	}
}

func (c *CoverageApp) runDefault() error {
	var err error
	var out string

	// get expected coverage from env
	coverageRaw := os.Getenv("GO_COVERAGE")
	log.Println("GO_COVERAGE:", coverageRaw)
	if coverageRaw == "" {
		return fmt.Errorf("GO_COVERAGE env var is not set")
	}

	out, err = c.Writer.Create()
	if err != nil {
		fmt.Println(out)
		return fmt.Errorf("failed to create cover profile: %v", err)
	}

	// get tests coverage from coverage profile
	coverage, err := c.Reader.Read()
	if err != nil {
		return fmt.Errorf("failed to get coverage: %v", err)
	}
	log.Println(coverage)

	// get percents from coverage and coverageRaw
	coveragePercentRaw, err := parseCoverageRaw(coverageRaw)
	if err != nil {
		return fmt.Errorf("failed to parse coverage percent raw: %v", err)
	}

	coveragePercent, err := c.Reader.ParseCoverage(coverage)
	if err != nil {
		return fmt.Errorf("failed to parse coverage percent: %v", err)
	}

	// compare expected coverage with actual coverage
	log.Println("current coverage:", coveragePercent, "expected coverage:", coveragePercentRaw)

	if coveragePercentRaw > coveragePercent {
		return fmt.Errorf("coverage is less than expected, want: %v, got: %v", coveragePercentRaw, coveragePercent)
	}

	return nil
}

func (c *CoverageApp) runRecursive(excludedPaths map[string]bool) error {
	err := filepath.Walk("./", func(path string, info fs.FileInfo, err error) error {
		if info.IsDir() {
			if _, err := os.Stat(path + "/go.mod"); err == nil {
				log.Println("")
				log.Println("try run tests for path:", path)

				if excludedPaths != nil {
					if excludedPaths[path] {
						log.Println("skipped tests for path:", path)
						return nil
					}
				}

				currentDir, err := os.Getwd()

				if err != nil {
					return fmt.Errorf("error during os.Getwd(): %v", err)
				}

				err = os.Chdir(path)

				if err != nil {
					return fmt.Errorf("error during os.Chdir(): %v", err)
				}

				err = godotenv.Load()

				if err != nil {
					log.Println("error loading .env file in path:", path)
				}

				err = c.runDefault()

				if err != nil {
					log.Println(err)
				}

				os.Unsetenv("GO_COVERAGE")
				err = os.Chdir(currentDir)

				if err != nil {
					return fmt.Errorf("error during os.Chdir(): %v", err)
				}
			}
		}

		return nil
	})

	return err
}

// parseCoverageRaw extract percent from the coverage raw string
func parseCoverageRaw(coveragePercentRaw string) (float64, error) {
	coveragePercentRaw = strings.TrimSuffix(coveragePercentRaw, "%")
	coveragePercent, err := strconv.ParseFloat(coveragePercentRaw, 64)
	if err != nil {
		return 0, fmt.Errorf("failed to parse coverage percent: %v", err)
	}
	return coveragePercent, nil
}
