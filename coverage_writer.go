package gocovci

import (
	"log"
	"os/exec"
)

type CoverageWriter interface {
	// Create tests coverage.
	Create() (string, error)
}

type FileCoverageWriter struct {
}

func NewFileCoverageWriter() *FileCoverageWriter {
	return &FileCoverageWriter{}
}

// Create file with tests coverage
func (f *FileCoverageWriter) Create() (string, error) {
	cmd := exec.Command("go", "test", "-coverpkg=./...", "-coverprofile=coverage.out", "-covermode=atomic", "./...")
	output, err := cmd.CombinedOutput()
	if err != nil {
		log.Println(string(output))
		return "", err
	}

	return string(output), nil
}
