package gocovci

import (
	"fmt"
	"os/exec"
	"strconv"
	"strings"
)

type CoverageReader interface {
	// Read returns the coverage profile as a string.
	Read() (string, error)
	// Coverage returns the coverage percentage as a string.
	Coverage() (string, error)
	// ParseCoverage extract percent from the coverage string
	ParseCoverage(string) (float64, error)
}

type FileCoverageReader struct {
}

func NewFileCoverageReader() *FileCoverageReader {
	return &FileCoverageReader{}
}

// Read tests coverage profile file
func (f *FileCoverageReader) Read() (string, error) {
	cmd := exec.Command("go", "tool", "cover", "-func", "coverage.out")
	output, err := cmd.CombinedOutput()
	if err != nil {
		return "", err
	}

	return string(output), nil
}

// Coverage returns the coverage percentage as a string.
func (f *FileCoverageReader) Coverage() (string, error) {
	coverage, err := f.Read()
	if err != nil {
		return "", fmt.Errorf("failed to read coverage profile: %v", err)
	}

	coverage = strings.TrimSpace(coverage)
	coverage = strings.Replace(coverage, "\r\n", "\n", -1)
	lines := strings.Split(coverage, "\n")

	if len(lines) < 1 {
		return "", fmt.Errorf("coverage profile is empty")
	}

	if !strings.Contains(lines[len(lines)-1], "total:") {
		return "", fmt.Errorf("coverage profile is invalid")
	}

	coverage = lines[len(lines)-1]
	return coverage, nil
}

// ParseCoverage extract percent from the coverage string
func (f *FileCoverageReader) ParseCoverage(coverage string) (float64, error) {
	totalParts := strings.Fields(coverage)
	outputPercentRaw := strings.TrimSuffix(totalParts[len(totalParts)-1], "%")

	if outputPercentRaw == "" {
		return 0, fmt.Errorf("coverage outputPercentRaw is empty")
	}

	outputPercent, err := strconv.ParseFloat(outputPercentRaw, 64)
	if err != nil {
		return 0, fmt.Errorf("failed to parse coverage percent: %v", err)
	}

	return outputPercent, nil
}
