package main

import (
	"github.com/joho/godotenv"
	"gitlab.com/valentinsubotka/gocovci/app"

	"log"
)

func main() {
	var err error
	_ = godotenv.Load()

	app := app.NewCoverageApp()
	err = app.Run()

	if err != nil {
		log.Fatal(err)
	}
}
